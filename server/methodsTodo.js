Meteor.methods({
	insertTodo: function(option){
		console.log(option);
		var id = Todolist.insert({ title: option.title, content: option.content});
		return id;
	},
	deleteTodo: function(option){
		console.log(option);
		var id = Todolist.remove({_id: option.idTodo});
	},
	updateTodo: function(option){
		console.log(option);
		var update = Todolist.update({_id: option.idTodo}, {$set:{title: option.title, content: option.content}} );
		return update;
	}
});
