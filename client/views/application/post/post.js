Template.post.onCreated(function(){
	Session.set("postVisibility", true);
});

Template.post.helpers({
	hide: function(){
		return Session.get("postVisibility");
	}
});
