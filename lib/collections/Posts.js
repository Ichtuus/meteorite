import SimpleSchema from 'simpl-schema';

Posts = new Mongo.Collection('posts');

Posts.attachSchema(new SimpleSchema({
	title: {
		type: String,
		max: 250
	},
	author: {
		type: String,
		max: 250
	},
	content: {
		type: String,
		max: 2000
	},
	createdAt: {
		type: Date,
		autoValue: function(){
			if(this.isInsert){
				return new Date;
			}
			else if(this.isUpsert){
				return {$setOnInsert: new Date};
			}
			else{
				this.unset();
			}
		}
	}
}));
