Router.configure({
	layoutTemplate: 'mainLayout'
});

Router.route("/", {
	name: "accueil",
	waitOn: function(){
		return Meteor.subscribe("allPostHeaders");
	}
});

Router.route("/posts", {
	name: "posts",
	waitOn: function(){
		return Meteor.subscribe("allPostHeaders");
	}
});


Router.route("/todo", {
	name: "todo",
	waitOn: function(){
		return Meteor.subscribe("allTodoHeaders");
	}
});
