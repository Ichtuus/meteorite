Template.todo.onCreated(function(){
	Session.set("mode", "hide");
	Session.set("upId", false);
});

Template.todo.helpers({
	todoAll: function(){
    	return Todolist.find({});
	},
  	mode: function(option){
	  	return (option == Session.get("mode"));
  	},
	form: function(){
		return Todolist.findOne({_id: Session.get("upId")});
	}
});

Template.todo.events({
	'submit #form_add_todo': function(event, template){
    	event.preventDefault();
		console.log("envoie");
    	var option = {
				title: event.target.title.value,
				content: event.target.content.value
		};
  		console.log(option)
    	Meteor.call("insertTodo", option, function(error, result){
			if(error){
				console.log("error", error.reason);
			}
			else{
				if(result){
					toastr.success("Todo bien enregistré");
					console.log(result);
				}
				else{
					toastr.error("L'enregistrement n'a pas fonctionné")
				}
			}
		});
  	},
  "click .opop": function(event, template){
	console.log("opop");
	Session.set('mode', "new");
  },
  "click .close": function(event, template){
	console.log("close");
	Session.set('mode',"hide");
  },
  "click .suppr": function(event, template){
	console.log("suppr");
	var option = {
		idTodo: $(event.currentTarget).data("id")
	}
	Meteor.call("deleteTodo", option, function(error, result){
		if(error){
			console.log("error", error.reason);
		}
		if(result){
			console.log(result);
			}
		});
	},
	"click .upda": function(event, template){
		console.log("upda");
		Session.set('mode','up');
		Session.set('upId', $(event.currentTarget).data('id'));
	},

	"submit #form_up_todo": function(event, template){
		event.preventDefault();
		var option = {
			id: event.target.id.value,
			title: event.target.title.value,
			content: event.target.content.value
		}
		Meteor.call("updateTodo", option, function(error, result){
			if(error){
				console.log("error", error.reason);
			}
			if(result){
				console.log(result);
				Session.set("mode", "hide");
			}
		});
	}

});
