Template.posts.onCreated(function(){
	Session.set("mode", "display");
	Session.set("updateId", false);
});

Template.posts.helpers({
	postsAll: function(){
		return Posts.find({});
	},
	hide: function(){
		return Session.get("mode");
	},
	isMode: function(option){
		return (option == Session.get("mode"));
	},
	form: function(){
		return Posts.findOne({_id: Session.get("updateId")});
	}
});

Template.posts.events({
	"submit #form_add": function(event, template){
		event.preventDefault();
		console.log("envoie");
		var option = {
			title: event.target.title.value,
			content: event.target.content.value
		};
		console.log(option);
		Meteor.call("insertPost", option, function(error, result){
			if(error){
				console.log("error", error.reason);
			}
			else{
				if(result){
					console.log(result);
					Session.set('affiche', true);
				}
			}
		});
	},

	"submit #form_update": function(event, template){
		event.preventDefault();
		console.log("envoie");
		var option = {
			id: event.target.id.value,
			title: event.target.title.value,
			content: event.target.content.value
		};
		console.log(option);
		Meteor.call("updatePost", option, function(error, result){
			if(error){
				console.log("error", error.reason);
			}
			else{
				if(result){
					console.log(result);
					Session.set('mode', 'display');
				}
			}
		});
	},

	"click .newform": function(event, template){
		console.log("newform");
		Session.set('mode', "add");
	},

	"click .close": function(event,template){
		console.log("close");
		Session.set('mode', "display");
	},

	"click .delete": function(event, template){
		console.log("delete");
		var option = {
			cestmonid: $(event.currentTarget).data("id")
		}
		Meteor.call("deletePost", option, function(error, result){
			if(error){
				console.log("error", error.reason);
			}
			if(result){
				 console.log(result);
			}
		});
	},

	"click .update": function(event, template){
		console.log("update");
		Session.set('mode', "update");
		Session.set('updateId', $(event.currentTarget).data('id'));
	}
});
